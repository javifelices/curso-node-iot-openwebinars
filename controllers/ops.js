'use strict';

function health(req, res) {
	res.status(200).send({ message: 'UP' });
}

module.exports = {
	health
};
